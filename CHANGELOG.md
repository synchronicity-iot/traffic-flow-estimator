# 1.0 (2019-06-13)

### Features

- REST API that returns the value of the estimations
- Context Data Manager (getting data from underlying Orion CBs)
- Historical data dumps - Synchronicity-specific API (~STH Comet)
- Static OAuth2 authentication (for the underlying Orion CBs)
- Elasticsearch for persistence
- Data aggregation for model training
- AI Engine (Keras + Scikit learn) - AI as a Service operation
- Prediction models: LSTM, Phased-LSTM and Dense Fully Connected Layers
- IoT AI Engine development split into [another repo](https://gitlab.atosresearch.eu/ari/iot-ai-engine)
- Plug-and-play Docker-based (and docker-compose) deployment
- Independent and file-based performance logging
- Dashboard mock-up (to be deprecated)
- (Optional) Orion CB included into the ecosystem
- (Optional) Kibana component for data visualization and monitoring